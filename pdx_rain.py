import requests
import re
import urllib.request

url = 'http://or.water.usgs.gov/non-usgs/bes/sunnyside.rain'
#r = requests.get('http://or.water.usgs.gov/non-usgs/bes/sunnyside.rain')

def open_url(url):
    with urllib.request.urlopen(url) as rain:
        lines = [byte_line.decode('utf-8') for byte_line in rain]
        return lines

def clean_lines(lines):
    # Replace new line headers
    lines = [line.replace("\n", "") for line in lines]
    # Remove any line of string that does not begin with a date
    new_lines = [line for line in lines if re.match('\d.-\w..-\d{4}', line)]
    # convert list to dict, storing key as date
    return new_lines

def match_line(line):
    new_dict = re.match(r"(?P<date>\d.-\w..-\d...)(?P<rain>\s+\S+)", line).groupdict()
    if new_dict != None and len(new_dict) != 0:
        return new_dict

list_lines = clean_lines(open_url(url))
master_list = list(map(match_line, list_lines))

def max_rain(master):
    r_val_list = []
    d_val_list = []
    for x in master:
        d_val_list.append(x["date"])
        r_val_list.append(x["rain"])
    zipped_list = zip(d_val_list, r_val_list)
    zipped_list = list(zipped_list)
    final_list = max(zipped_list, key=lambda t: t[1])
    date, rain = final_list
    rain = int(rain)*0.01
    print("On {} Portland had {} inches of rain.".format(date, rain))

max_rain(master_list)
